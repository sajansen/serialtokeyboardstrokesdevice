#include <Arduino.h>

// Some settings
#define ROWS_AMOUNT   4
#define COLS_AMOUNT   4
#define BAUD_RATE     115200
#define DATA_UPDATE_INTERVAL    300   // in milliseconds
#define DATA_UPDATE_INIT_BYTE   0xff

#define FEEDBACK_BUZZER_LANDING_DURATION  1000    // in milliseonds
#define FEEDBACK_BUZZER_CRASHED_DURATION  3500    // in milliseonds

// Pin setup
const uint8_t row_pins[ROWS_AMOUNT] = {2, 3, 4, 5};
const uint8_t col_pins[COLS_AMOUNT] = {6, 7, 8, 9};
const uint8_t potmeter_spoiler_pin = A0;
const uint8_t potmeter_rudder_left_pin = A1;
const uint8_t potmeter_rudder_right_pin = A2;
const uint8_t feedback_buzzer_pin = 10;

// Mapping
const uint8_t mapping[ROWS_AMOUNT][COLS_AMOUNT] = {
        {0x00, 0x01, 0x02, 0x03},
        {0x04, 0x05, 0x06, 0x07},
        {0x08, 0x09, 0x0a, 0x0b},
        {0x0c, 0x0d, 0x0e, 0x0f}
};
const uint8_t data_spoiler_id = 0x10;
const uint8_t data_rudder_id = 0x11;
const uint8_t data_brake_left_id = 0x12;
const uint8_t data_brake_right_id = 0x13;

// The to be requested data (in order)
struct SimulatorData {
    uint16_t PLANE_ON_GROUND;
    int16_t CRASHED;
    uint8_t OVERSPEED;
    uint16_t CLOUD_TURBULENCE;
    int32_t GEAR_ACTUAL_RIGHT;
//  int32_t GROUND_SPEED;
//  uint8_t NAVIGATION_LIGHTS;
//  uint8_t STROBE_LIGHTS;
//  uint16_t BRAKE_LEFT;
//  uint16_t BRAKE_RIGHT;
};

// Other constants
const uint16_t potmeter_left_lower_bound = 24;
const uint16_t max_spoiler_value = 16384;
const uint16_t min_spoiler_value = 5636;
const uint16_t max_rudder_value = 16384;
const uint16_t min_rudder_value = 0;


/**********************************
 * Do not edit variables below
 */

unsigned long last_data_update_time = 0;
struct SimulatorData simulatorData = {0};

// Updated values
uint16_t potmeter_spoiler_value = 0;
uint16_t potmeter_rudder_left_value = 0;
uint16_t potmeter_rudder_right_value = 0;
uint8_t is_plane_on_ground = 0;
uint8_t is_crashed = 0;
uint16_t current_rudder_value = 0;
uint8_t brake_force_left = 0;
uint8_t brake_force_right = 0;
unsigned long feedback_buzzer_stop_time = 0;
unsigned long next_turbulent_time = 0;

void serialFlush() {
    while (Serial.available() > 0) {
        Serial.read();
    }
}

void debounceButton(uint8_t col) {
    while (!digitalRead(col_pins[col])) {
        delay(75);
    }
}

void turnOnFeedbackBuzzer() {
    digitalWrite(feedback_buzzer_pin, HIGH);
}

void startFeedbackBuzzer(unsigned long duration) {
    feedback_buzzer_stop_time = millis() + duration;
    turnOnFeedbackBuzzer();
}

void updateFeedbackBuzzer() {
    if (millis() > feedback_buzzer_stop_time) {
        digitalWrite(feedback_buzzer_pin, LOW);
    }
}

void handleLanding(SimulatorData *simulatorData) {
    if (is_plane_on_ground == simulatorData->PLANE_ON_GROUND) return;
    is_plane_on_ground = simulatorData->PLANE_ON_GROUND;

    if (!is_plane_on_ground) return;
    // Plane just landed
    startFeedbackBuzzer(FEEDBACK_BUZZER_LANDING_DURATION);
}

void handleCrash(SimulatorData *simulatorData) {
    if (simulatorData->CRASHED == is_crashed) return;
    is_crashed = simulatorData->CRASHED;

    if (!is_crashed) return;
    startFeedbackBuzzer(FEEDBACK_BUZZER_CRASHED_DURATION);
}

void handleOverspeed(SimulatorData *simulatorData) {
    if (!simulatorData->OVERSPEED) return;
    startFeedbackBuzzer(DATA_UPDATE_INTERVAL);
}

void handleTurbulence(SimulatorData *simulatorData) {
    if (!simulatorData->CLOUD_TURBULENCE) return;

    const unsigned long now = millis();
    if (now < next_turbulent_time) return;

    startFeedbackBuzzer(((pow(2, random(0, 1500) / 1000.0) - 0.8) * 1000));

    next_turbulent_time = now + ((pow(2, random(0, 5000) / 1000.0) - 1) * 1000);
}

void handleGearDown(SimulatorData *simulatorData) {
    if (simulatorData->GEAR_ACTUAL_RIGHT == 0 || simulatorData->GEAR_ACTUAL_RIGHT > 16380) return;
    startFeedbackBuzzer(DATA_UPDATE_INTERVAL);
}

void process_data(SimulatorData *simulatorData) {
//  digitalWrite(LED_BUILTIN, simulatorData->LANDING_LIGHTS);

    handleLanding(simulatorData);
    handleCrash(simulatorData);
    handleOverspeed(simulatorData);
    handleTurbulence(simulatorData);
    handleGearDown(simulatorData);
}

uint8_t update_data(SimulatorData *simulatorData) {
    serialFlush();

    // Request for an update
    Serial.write(DATA_UPDATE_INIT_BYTE);

    // Retrieve the update
    size_t bytes_read = Serial.readBytes((char *) simulatorData, sizeof(SimulatorData));
    return bytes_read == sizeof(SimulatorData);
}

void handleDateRetrieving() {
    if (last_data_update_time + DATA_UPDATE_INTERVAL > millis()) return;

    last_data_update_time = millis();

    if (!update_data(&simulatorData)) return;

    process_data(&simulatorData);
}

void serialWriteBytes(int32_t value, uint8_t length) {
    while (length > 0) {
        Serial.write((uint8_t) (value >> --length * 8));
    }
}

void readAndSendButtonMatrix() {
    for (uint8_t row = 0; row < ROWS_AMOUNT; row++) {
        digitalWrite(row_pins[row], LOW);
        for (uint8_t col = 0; col < COLS_AMOUNT; col++) {
            if (digitalRead(col_pins[col]) != 0) continue;
            Serial.write(mapping[row][col]);
            debounceButton(col);
        }
        digitalWrite(row_pins[row], HIGH);
    }
}

void readAndSendSpoilerPosition() {
    const uint16_t potmeter_reading = analogRead(potmeter_spoiler_pin);
    if (potmeter_reading == potmeter_spoiler_value) return;

    potmeter_spoiler_value = potmeter_reading;

    int32_t spoiler_value =
            (int32_t) (max_spoiler_value - min_spoiler_value) * analogRead(potmeter_spoiler_pin) / 1023 +
            min_spoiler_value;
    if (spoiler_value < min_spoiler_value + 25) {
        spoiler_value = 0;
    }

    Serial.write(data_spoiler_id);
    serialWriteBytes(spoiler_value, 4);
    delay(100);    // debounce
}

void readAndSendBrakeCommando(uint16_t potmeter_reading_left, uint16_t potmeter_reading_right) {
    const bool new_value_left = potmeter_reading_left == 0;
    const bool new_value_right = potmeter_reading_right == 0;

    if (new_value_left != brake_force_left) {
        brake_force_left = new_value_left;

        Serial.write(data_brake_left_id);
        if (brake_force_left) {
            serialWriteBytes(16383, 2);
        } else {
            serialWriteBytes(0, 2);
        }
    }

    if (new_value_right != brake_force_right) {
        brake_force_right = new_value_right;

        Serial.write(data_brake_right_id);
        if (brake_force_right) {
            serialWriteBytes(16383, 2);
        } else {
            serialWriteBytes(0, 2);
        }
    }
}

void readAndSendRudderPosition(uint16_t potmeter_reading_left, uint16_t potmeter_reading_right) {
    if (potmeter_reading_left == potmeter_rudder_left_value &&
        potmeter_reading_right == potmeter_rudder_right_value)
        return;
    potmeter_rudder_left_value = potmeter_reading_left;
    potmeter_rudder_right_value = potmeter_reading_right;

    const int16_t rudder_difference = potmeter_rudder_left_value - potmeter_rudder_right_value;

    int16_t new_rudder_value = 0;
    if (rudder_difference > 15 || rudder_difference < -15) {
        new_rudder_value =
                (int32_t) (max_rudder_value - min_rudder_value) * rudder_difference / 1023 + min_rudder_value;
    }

    if (new_rudder_value == current_rudder_value) return;
    current_rudder_value = new_rudder_value;

    Serial.write(data_rudder_id);
    serialWriteBytes(current_rudder_value, 2);
    delay(50);    // debounce
}

void readAndSendFootPedals() {
    uint16_t potmeter_reading_left = analogRead(potmeter_rudder_left_pin);
    uint16_t potmeter_reading_right = analogRead(potmeter_rudder_right_pin);

    // The right potmeter has a lower ceiling and the left one must also correct for that
    // Scale left rudder up from potmeter_left_lower_bound
    potmeter_reading_left = max(potmeter_left_lower_bound, potmeter_reading_left);
    potmeter_reading_left = (uint32_t) 1023 * (potmeter_reading_left - potmeter_left_lower_bound)
                            / (1023 - potmeter_left_lower_bound);

    readAndSendBrakeCommando(potmeter_reading_left, potmeter_reading_right);
    readAndSendRudderPosition(potmeter_reading_left, potmeter_reading_right);
}

void handleDataSending() {
    readAndSendButtonMatrix();
    readAndSendSpoilerPosition();
    readAndSendFootPedals();
}

void handleActuators() {
    updateFeedbackBuzzer();
}

void setup() {
    Serial.begin(BAUD_RATE);

    for (uint8_t row_pin : row_pins) {
        pinMode(row_pin, OUTPUT);
        digitalWrite(row_pin, HIGH);
    }

    for (uint8_t col_pin : col_pins) {
        pinMode(col_pin, INPUT);
        digitalWrite(col_pin, HIGH);
    }

    pinMode(potmeter_spoiler_pin, INPUT);
    pinMode(potmeter_rudder_left_pin, INPUT);
    pinMode(potmeter_rudder_right_pin, INPUT);

    pinMode(feedback_buzzer_pin, OUTPUT);
}

void loop() {
    handleDataSending();
    handleDateRetrieving();
    handleActuators();
}
